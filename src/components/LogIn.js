import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const LogIn = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoggedIn, setLoggedIn] = useState(false);
  const navigate = useNavigate();

  const handleLogIn = async () => {
    const response = await fetch("http://localhost:3001/api/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username, password }),
    });

    const data = await response.json();

    if (data.success) {
      setLoggedIn(true);
      navigate.push("/");
    } else {
      alert("Invalid credentials");
    }
  };

  return (
    <div>
      {isLoggedIn ? (
        <p>You are already logged in. Redirecting...</p>
      ) : (
        <>
          <h2 style={{ margin: "0 auto", textAlign: "center" }}>Log In</h2>
          <div className="wrapper">
            <input
              type="text"
              placeholder="Username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <input
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <button onClick={handleLogIn}>Log In</button>
          </div>
        </>
      )}
    </div>
  );
};

export default LogIn;
