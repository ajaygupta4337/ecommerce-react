import React, { useEffect, useState } from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Cardsdata from "./CardData";
import "./Style.css";
import { useDispatch } from "react-redux";
import { ADD } from "../redux/actions/action";
import { height } from "@mui/system";

const Cards = ({ data: propData }) => {
  const [data, setData] = useState(Cardsdata);
  let searchStr = null;

  const dispatch = useDispatch();

  const send = (e) => {
    dispatch(ADD(e));
  };

  useEffect(() => {
    if (searchStr) {
      let newData = propData.filter(
        (obj) => obj.rname.toLowerCase().indexOf(searchStr.toLowerCase()) > -1
      );
      setData(newData);
    } else {
      setData(propData);
    }
  }, [searchStr, propData]);

  return (
    <div className="container mt-3">
      <div className="row d-flex justify-content-center align-items-center">
        {data.map((element) => {
          // console.log("Data:", data);
          return (
            <>
              <Card
                key={element.id}
                style={{ width: "22rem", border: "none" }}
                className="mx-2 mt-4 card_style"
              >
                <Card.Img
                  variant="top"
                  src={element.imgdata}
                  style={{ height: "16rem" }}
                  className="mt-3"
                />

                <div className="card_body">
                  <div className="upper_data d-flex justify-content-between align-items-center">
                    <h4 className="mt-2">{element.rname}</h4>
                    <span>{element.rating}&nbsp;★</span>
                  </div>

                  <div className="lower_data d-flex  justify-content-between">
                    <h5>{element.address}</h5>
                    <span>{element.price}</span>
                  </div>

                  <div className="extra"></div>

                  <div className="last_data d-flex justify-content-between align-items-center">
                    <img src={element.arrimg} className="limg" alt="" />
                    <p>{element.somedata}</p>
                    <img src={element.delimg} className="laimg" alt="" />
                  </div>

                  <div className="button_div d-flex justify-content-center">
                    <Button
                      variant="primary"
                      onClick={() => send(element)}
                      className="col-lg-12"
                    >
                      Add to Cart
                    </Button>
                  </div>
                </div>
                {/* <Card.Body>
                                        <Card.Title>{element.rname}</Card.Title>
                                        <Card.Text>
                                            Price: ₹{element.price}
                                        </Card.Text>
                                        <div className='button_div d-flex justify-content-center'>
                                            <Button variant="primary" 
                                            onClick={()=> send(element)}
                                            className='col-lg-12'>Add to Cart</Button>
                                        </div>
                                    </Card.Body> */}
              </Card>
            </>
          );
        })}
      </div>
    </div>
  );
};

export default Cards;
