import React, { useEffect, useState } from "react";
import Cardsdata from "./CardData";
import "./Style.css";
import Form from "react-bootstrap/Form";
import Cards from "./Cards";
import { useNavigate } from "react-router-dom";
const Search = () => {
  const [fdata, setFdata] = useState(Cardsdata);
  // console.log(fdata);

  const changeData = (e) => {
    const getChangeData = e.toLowerCase();

    if (getChangeData === "") {
      setFdata(Cardsdata);
    } else {
      const storeData = Cardsdata.filter((ele) => {
        const dishName = ele.rname.toLowerCase();
        return dishName.includes(getChangeData);
      });

      console.log("Filtered Data:", storeData); // Add this line

      setFdata(storeData);
    }
  };

  const zomatologo =
    "https://b.zmtcdn.com/web_assets/b40b97e677bc7b2ca77c58c61db266fe1603954218.png";

  useEffect(() => {
    setTimeout(() => {
      setFdata(Cardsdata);
    }, 3000);
    setFdata(Cardsdata);
  }, []);

  return (
    <>
      <Form className="d-flex justify-content-center align-items-center mt-3">
        <Form.Group className=" mx-2 col-lg-4" controlId="formBasicEmail">
          <Form.Control
            type="text"
            onChange={(e) => changeData(e.target.value)}
            placeholder="Search Restaurant"
          />
        </Form.Group>
      </Form>

      <section className="iteam_section mt-4 container">
        <h2 className="px-4" style={{ fontWeight: 400, textAlign: "center" }}>
          Restaurants in Mumbai Open now
        </h2>

        <div className="row mt-2 d-flex justify-content-around align-items-center">
          {fdata && <Cards data={fdata} />}
        </div>
      </section>
    </>
  );
};

export default Search;
