// App.jsx
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./components/Header";
import CardsDetails from "./components/CardsDetails";
import Cards from "./components/Cards";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Search from "./components/Search";
import SignIn from "./components/SignIn";
import LogIn from "./components/LogIn";

function App() {
  return (
    <Router>
      <Header />
      <Routes>
        <Route path="/" element={<Search />} />
        <Route path="/signin" element={<SignIn />} />
        <Route path="/login" element={<LogIn />} />
        <Route path="/cart/:id" element={<CardsDetails />} />
        <Route path="/home" element={<Cards />} />
      </Routes>
    </Router>
  );
}

export default App;
